var gulp = require('gulp'),
	less = require('gulp-less'),
	concat = require('gulp-concat');

gulp.task('build', function () {
	gulp.src(['./src/style/reset.less', './src/style/controls.less', './src/style/main.less'])
		.pipe(less())
		.pipe(concat('style.css'))
		.pipe(gulp.dest('build'));

	gulp.src('./src/*.html').pipe(gulp.dest('build'));
	gulp.src('./src/img/*.*').pipe(gulp.dest('build/img'));
});	

gulp.task('default', function(){
	gulp.watch(['./src/style/*.less', './src/index.html'], ['build']);
});